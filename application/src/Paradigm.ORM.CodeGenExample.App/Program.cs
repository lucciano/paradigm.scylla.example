﻿using System;
using System.Threading.Tasks;

namespace Paradigm.ORM.CodeGenExample.App
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var startup = new Startup();
            startup.Configure();
            startup.RunAsync().Wait();

            System.Console.ReadKey();
        }
    }
}
