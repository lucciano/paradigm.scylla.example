﻿using Microsoft.Extensions.DependencyInjection;
using Paradigm.ORM.CodeGenExample.Domain;
using Paradigm.ORM.CodeGenExample.Domain.DataAccess;
using Paradigm.ORM.CodeGenExample.Domain.Entities;
using Paradigm.ORM.CodeGenExample.Domain.Interfaces;
using Paradigm.ORM.CodeGenExample.Domain.Mappers;
using Paradigm.ORM.Data.DatabaseAccess.Generic;
using Paradigm.ORM.Data.Mappers.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paradigm.ORM.CodeGenExample.App
{
    internal class Startup
    {
        private IServiceProvider ServiceProvider { get; set; }

        public void Configure()
        {
            var serviceCollection = new ServiceCollection();

            serviceCollection.AddSingleton<ICassandraConnector, CassandraConnector>();
            serviceCollection.AddTransient<ITrackingData, TrackingData>();
            serviceCollection.AddTransient<TrackingData>();
            serviceCollection.AddTransient<ITrackingDataDatabaseAccess, TrackingDataDatabaseAccess>();
            serviceCollection.AddTransient<ITrackingDataDatabaseReaderMapper, TrackingDataDatabaseReaderMapper>();
            serviceCollection.AddTransient<IDatabaseAccess<TrackingData>, TrackingDataDatabaseAccess>();
            serviceCollection.AddTransient<IDatabaseReaderMapper<TrackingData>, TrackingDataDatabaseReaderMapper>();
            this.ServiceProvider = serviceCollection.BuildServiceProvider();
        }

        public async Task RunAsync()
        {
            var random = new Random();
            using (var connector = this.ServiceProvider.GetRequiredService<ICassandraConnector>())
            {
                connector.Initialize("Contact Points=192.168.2.221;Port=9042");
                await connector.OpenAsync();

                if (connector.IsOpen())
                {
                    System.Console.WriteLine("We are connected ...");

                    var databaseAccess = this.ServiceProvider.GetRequiredService<ITrackingDataDatabaseAccess>();
                    var tracking1 = this.ServiceProvider.GetRequiredService<TrackingData>();

                    tracking1.FirstName = "James";
                    tracking1.LastName = "Howlett";
                    tracking1.Location = "Canada";
                    tracking1.Heat = 10 * random.NextDouble();
                    tracking1.Speed = 10 * random.NextDouble();
                    tracking1.Timestamp = DateTimeOffset.Now;
                    tracking1.TelepathyPowers = 0;

                    await databaseAccess.InsertAsync(tracking1);

                    var trackings = await databaseAccess.SelectAsync();
                    foreach(var tracking in trackings)
                    {
                        System.Console.WriteLine($"{tracking.FirstName} {tracking.LastName} ({tracking.Location}) : [seed={tracking.Speed}, heat={tracking.Heat}]");
                    }

                    foreach (var tracking in trackings)
                    {
                        await databaseAccess.DeleteAsync(tracking);
                    }

                    if (!(await databaseAccess.SelectAsync()).Any())
                    {
                        System.Console.WriteLine("All entries have been deleted");
                    }
                }

                await connector.CloseAsync();
            }
        }
    }
}
